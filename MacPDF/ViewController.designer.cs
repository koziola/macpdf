// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ProjectDays2017MacApp
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		AppKit.NSSegmentedControl DisplayModeControl { get; set; }

		[Outlet]
		AppKit.NSTextField FilePathValue { get; set; }

		[Outlet]
		PdfKit.PdfView PDFView { get; set; }

		[Outlet]
		PdfKit.PdfThumbnailView ThumbnailView { get; set; }

		[Outlet]
		AppKit.NSComboBox ZoomControl { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (DisplayModeControl != null) {
				DisplayModeControl.Dispose ();
				DisplayModeControl = null;
			}

			if (FilePathValue != null) {
				FilePathValue.Dispose ();
				FilePathValue = null;
			}

			if (PDFView != null) {
				PDFView.Dispose ();
				PDFView = null;
			}

			if (ZoomControl != null) {
				ZoomControl.Dispose ();
				ZoomControl = null;
			}

			if (ThumbnailView != null) {
				ThumbnailView.Dispose ();
				ThumbnailView = null;
			}
		}
	}
}
