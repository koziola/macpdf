﻿using System;
using System.Collections.Generic;

namespace ProjectDays2017MacApp
{
    public struct OpenDialogResult
    {
		public bool OK;
		public List<string> FilePaths;
    }
}
