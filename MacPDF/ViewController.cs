﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppKit;
using Foundation;

namespace ProjectDays2017MacApp
{
    public partial class ViewController : NSViewController
    {
        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Do any additional setup after loading the view.
            ConfigureViews();
        }

        private void ConfigureViews()
        {
            ConfigurePDFView();
            ConfigureFileLabel();
            ConfigureDisplayModeControl();
            ConfigureZoomControl();
            ConfigureThumbnailView();
        }

        private void ConfigurePDFView()
        {
            PDFView.TranslatesAutoresizingMaskIntoConstraints = false;
            PDFView.DisplayMode = PdfKit.PdfDisplayMode.SinglePageContinuous;
            PDFView.OpenPdf += HandlePDFOpened;
        }

        private void ConfigureFileLabel()
        {
            FilePathValue.TranslatesAutoresizingMaskIntoConstraints = false;
            FilePathValue.LineBreakMode = NSLineBreakMode.TruncatingTail;
            FilePathValue.UsesSingleLineMode = true;
            FilePathValue.StringValue = string.Empty;
        }

        private void ConfigureDisplayModeControl()
        {
            DisplayModeControl.Enabled = false;
            DisplayModeControl.SelectedSegment = 1;
            DisplayModeControl.Activated += (sender, e) => 
            {
                var selectedSegment = DisplayModeControl.SelectedSegment;
                switch(selectedSegment)
                {
                    case 0:
                        PDFView.DisplayMode = PdfKit.PdfDisplayMode.SinglePage;
                        break;
                    case 1:
                        PDFView.DisplayMode = PdfKit.PdfDisplayMode.SinglePageContinuous;
                        break;
                    case 2:
                        PDFView.DisplayMode = PdfKit.PdfDisplayMode.TwoUp;
                        break;
                    case 3:
                        PDFView.DisplayMode = PdfKit.PdfDisplayMode.TwoUpContinuous;
                        break;
                }
            };
        }

        private void ConfigureZoomControl()
        {
            ZoomControl.FloatValue = (float)PDFView.ScaleFactor;
            ZoomControl.Add(new NSObject[]
            {
                new NSNumber(.10),
                new NSNumber(.25),
                new NSNumber(.5),
                new NSNumber(1),
                new NSNumber(2),
                new NSNumber(5)
            });
            ZoomControl.Formatter = new NSNumberFormatter()
            {
                NumberStyle = NSNumberFormatterStyle.Percent
            };
            ZoomControl.Action = new ObjCRuntime.Selector("zoomSelected");
        }

        private void ConfigureThumbnailView()
        {
            ThumbnailView.TranslatesAutoresizingMaskIntoConstraints = false;
            ThumbnailView.PdfView = PDFView;
        }

        [Export ("zoomSelected")]
        private void ZoomSelected()
        {
            //var selectedZoom = NSNumber.FromObject(ZoomControl.SelectedValue);
            PDFView.ScaleFactor = ZoomControl.FloatValue;
        }

        public override NSObject RepresentedObject
        {
            get
            {
                return base.RepresentedObject;
            }
            set
            {
                base.RepresentedObject = value;
                // Update the view, if already loaded.
            }
        }

        [Export ("openDocument:")]
        public async void OpenDocument(NSObject sender)
        {
            var openResponse = await ShowOpenFileDialog(new List<string> { "pdf" }, false);
            if (openResponse.OK)
            {
                OpenPDF(openResponse.FilePaths.First());
            }
        }

        private Task<OpenDialogResult> ShowOpenFileDialog(IEnumerable<string> pAllowedExtensions,
                                                          bool pAllowMultipleSelection,
                                                          string pInitialDirectory = null,
                                                          string pDialogTitle = null)
        {
			var openPanel = new NSOpenPanel();
			openPanel.CanChooseFiles = true;
			openPanel.CanChooseDirectories = false;
			openPanel.CanCreateDirectories = false;
			openPanel.AllowsMultipleSelection = pAllowMultipleSelection;
			openPanel.AllowedFileTypes = pAllowedExtensions.Select((extension) => extension.Replace(".", string.Empty)).ToArray();
			if (pDialogTitle != null)
			{
				openPanel.Title = pDialogTitle;
				openPanel.Prompt = pDialogTitle;
			}
			var parentWindow = GetWindowForSheet();

			var taskCompletionSource = new TaskCompletionSource<OpenDialogResult>();
			openPanel.BeginSheet(parentWindow, (result) =>
			{
				var openResult = new OpenDialogResult()
				{
					OK = result == 1,
					FilePaths = openPanel.Urls.Select((url) => { return url.Path; }).ToList()
				};
				taskCompletionSource.TrySetResult(openResult);
			});
			return taskCompletionSource.Task;
        }

		private NSWindow GetWindowForSheet()
		{
			return NSApplication.SharedApplication.KeyWindow ?? NSApplication.SharedApplication.MainWindow;
		}

        private void OpenPDF(string pFilePath)
        {
            var url = NSUrl.FromFilename(pFilePath);
            if (url != null)
            {
                PDFView.Document = new PdfKit.PdfDocument(url);
                PDFView.NeedsDisplay = true;
                FilePathValue.StringValue = url.AbsoluteString;
                DisplayModeControl.Enabled = true;
            }
        }


		private void HandlePDFOpened(object sender, PdfKit.PdfViewActionEventArgs args)
		{
            var fieldsEnabled = PDFView.Document != null;
            DisplayModeControl.Enabled = fieldsEnabled;
            ZoomControl.Enabled = fieldsEnabled;
            if (!fieldsEnabled)
                FilePathValue.StringValue = string.Empty;
		}
    }
}
