﻿using System;
using AppKit;
using Foundation;
namespace ProjectDays2017MacApp
{
    [Register (nameof(MainWindowController))]
    public class MainWindowController : NSWindowController
    {

		public MainWindowController(IntPtr handle) : base(handle)
        {
		}

        public override void WindowDidLoad()
        {
            base.WindowDidLoad();
            Window.Appearance = NSAppearance.GetAppearance(NSAppearance.NameVibrantDark);
        }
    }
}
